/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//importer les classes et packages requis
package org.moutoh;

import java.util.Collections;  
import java.util.List;  
import java.util.stream.Collectors;  
  
import org.apache.commons.text.RandomStringGenerator;  
import static org.apache.commons.text.CharacterPredicates.DIGITS;

/**
 *
 * @author vagrant
 */
// crée la classe GeneratePassword pour générer un mot de passe aléatoire et sécurisé
public class GeneratePassword {
// démarrage de la méthode main()

    public static void main(String args[]) {

        // appelez la méthode generateSecurePassword() pour générer un mot de passe aléatoire à l'aide de RandomStringGenerator  
        String pass = generateSecurePassword();

        // affiche le mot de passe RandomStringGenerator
        System.out.println("Le mot de passe généré par RandomStringGenerator est:" + pass);

    }

    // crée la méthode generateSecurePassword() qui trouve le mot de passe sécurisé à 8 chiffres et le renvoie à la méthode main()
    public static String generateSecurePassword() {
        // génère une chaîne contenant 2 caractères spéciaux, 2 chiffres, 2 majuscules et 2 minuscules
        String demoPassword = generateRandomSpecialCharacters(2)
                .concat(generateRandomNumbers(2))
                .concat(generateRandomAlphabet(2, true))
                .concat(generateRandomAlphabet(2, false));
        // crée une liste de Char qui stocke tous les caractères, nombres et caractères spéciaux
        List<Character> listOfChar = demoPassword.chars()
                .mapToObj(data -> (char) data)
                .collect(Collectors.toList());
        // utilise la méthode shuffle() des Collections pour mélanger les éléments de la liste
        Collections.shuffle(listOfChar);
        // génère une chaîne aléatoire (mot de passe sécurisé) en utilisant la méthode list stream() et la méthode collect()
        String password = listOfChar.stream()
                .collect(StringBuilder::new, StringBuilder::append, StringBuilder::append)
                .toString();

        // renvoie le mot de passe RandomStringGenerator à la méthode main()  
        return password;
    }

    // crée la méthode generateRandomSpecialCharacters() qui renvoie une chaîne de caractères spéciaux de la longueur spécifiée
    public static String generateRandomSpecialCharacters(int length) {

        // génère une chaîne spéciale de caractères spéciaux en utilisant les méthodes Builder(), withinRange() et build()  
        RandomStringGenerator generator = new RandomStringGenerator.Builder().withinRange(33, 45)
                .build();

        return generator.generate(length);

    }
    // crée la méthode generateRandomNumbers() qui renvoie une chaîne de nombres de la longueur spécifiée

    public static String generateRandomNumbers(int length) {

        // génère une chaîne spéciale de nombres en utilisant les méthodes Builder(), withinRange(), filteredBy() et build()
        RandomStringGenerator generator = new RandomStringGenerator.Builder()
                .withinRange('0', 'z')
                .filteredBy(DIGITS)
                .build();
        return generator.generate(length);

    }

    // crée la méthode generateRandomAlphabet () qui renvoie soit une chaîne en majuscules, soit une chaîne en minuscules
    // de la longueur spécifiée basée sur la vérification de la variable booléenne  
    public static String generateRandomAlphabet(int length, boolean check) {

        String password;

        // pour une chaîne en minuscule
        if (check == true) {
            RandomStringGenerator generator = new RandomStringGenerator.Builder().withinRange('a', 'z')
                    .build();
            password = generator.generate(length);
        } // pour une chaîne en majuscule 
        else {
            RandomStringGenerator generator = new RandomStringGenerator.Builder().withinRange('A', 'Z')
                    .build();
            password = generator.generate(length);
        }

        return password;
    }
}
